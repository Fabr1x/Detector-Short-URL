#!/bin/bash
#Verificando Dependencias
if [ -x $PREFIX/bin/figlet ]; then
	clear
else
	apt install figlet -y
fi

if [ -x $PREFIX/bin/pv ]; then
	clear
else
	apt install pv -y
fi

#Banner

banner() {
clear
printf "\e[1;32m";figlet -t Detector
printf "\e[1;31m";figlet -t Short-Url
printf "\e[1;30m      Created by: \e[1;36mOtaku-dev  \e[0m\n"
sleep 2
}
banner
echo -e "\n Esta herramienta sirve para detectar la procedencia de un enlace acortado.\n"
sleep 2
printf "\e[1;34m[\e[1;35m?\e[1;34m]\e[1;33mPresiona Enter para Continuar"
read -r
banner
printf "\n\n\e[1;34m[\e[1;32m=\e[1;34m]\e[1;33mIngrese el enlace acortado: \e[1;37m"
read -r enlace
sleep 3
printf "\e[1;34m[\e[1;35m!\e[1;34m]\e[1;32mAnalizando el enlace...\e[0m\n"|pv -qL 12
sleep 3
printf "\e[1;34m[\e[1;32m+\e[1;34m]\e[1;33mMostrando Resultados\e[1;36m\n\n"
sleep 3

#Proceso

curl -sLI $enlace | grep -i location:

printf "\n\e[1;34m[\e[1;35m?\e[1;34m]\e[1;33mPara Salir escriba la letra \e[1;32ms \e[1;33mPara Continuar escribe la letra \e[1;32mc \e[1;33m: \e[1;37m"
read -r opcion

#Verificando

if [ "$opcion" == "s" ]; then
	banner
elif [ "$opcion" == "c" ]; then
	source 'detect-shorturl.sh'
else
	banner
fi

